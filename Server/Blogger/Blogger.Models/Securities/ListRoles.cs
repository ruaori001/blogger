﻿namespace Blogger.Models.Securities
{
    public static class ListRoles
    {
        public const string User = "User";
        public const string Admin = "Admin";
    }
}
