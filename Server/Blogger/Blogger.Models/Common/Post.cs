﻿using Blogger.Models.Enums;

namespace Blogger.Models.Common
{
    public class Post : BaseModel
    {
        public string? ShortContent { get; set; }
        public string? Content { get; set; }
        public string? UrlSlug { get; set; }
        public string? Author { get; set; } = "Anonymous author";

        public PostStatus postStatus { get; set; } = PostStatus.Pending;

        /// 1:n with Category
        public Guid CategoryId { get; set; }
        public virtual Category Category { get; set; } = new Category();

        /// <summary>
        /// n:n with Tag
        /// </summary>
        public virtual ICollection<Tag> Tags { get; set; } = new List<Tag>();
    }
}
