﻿using System.ComponentModel.DataAnnotations;

namespace Blogger.Models.Common
{
    public class BaseModel
    {
        [Key]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "{0} is required!")]
        public string Title { get; set; } = null!;

        public int AmountOfVisit { get; set; } = 0;
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public DateTime ModifiedAt { get; set; } = DateTime.Now;
    }
}
