﻿namespace Blogger.Models.Common
{
    public class Tag : BaseModel
    {
        public virtual ICollection<Post> Posts { get; set; } = new List<Post>();
    }
}
