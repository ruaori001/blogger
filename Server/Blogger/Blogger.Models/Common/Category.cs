﻿namespace Blogger.Models.Common
{
    public class Category : BaseModel
    {
        public string? UrlSlug { get; set; }

        ///n:1 with post
        public virtual ICollection<Post> Posts { get; set; } = new List<Post>();
    }
}
