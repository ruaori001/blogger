﻿namespace Blogger.Models.Enums
{
    public enum PostStatus
    {
        Available,
        Pending,
        Removed
    }
}
