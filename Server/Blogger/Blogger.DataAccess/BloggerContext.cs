﻿using Blogger.Models.Common;
using Microsoft.EntityFrameworkCore;

namespace Blogger.DataAccess
{
    public partial class BloggerContext : DbContext
    {
        public BloggerContext()
        {

        }

        public BloggerContext(DbContextOptions<BloggerContext> options) : base(options)
        {

        }

        public virtual DbSet<Category>? Categories { get; set; }
        public virtual DbSet<Post>? Posts { get; set; }
        public virtual DbSet<Tag>? Tags { get; set; }

        /* protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
         {
             optionsBuilder.UseSqlServer("Server=localhost\\SQLEXPRESS;Database=BloggerDB;uid=sa;pwd=123456;TrustServerCertificate=True;Trusted_Connection=True;MultipleActiveResultSets=true");
         }*/

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            DataSeeding(modelBuilder);
        }
    }
}
