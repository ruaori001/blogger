﻿using Blogger.Models.Common;
using Microsoft.EntityFrameworkCore;

namespace Blogger.DataAccess
{
    public partial class BloggerContext
    {
        private void DataSeeding(ModelBuilder modelBuilder) {
            List<Category> categories = new List<Category>
            {
                new Category
                {
                    Id = Guid.NewGuid(),
                    Title = "Technology",
                    UrlSlug = "technology"
                },
                new Category
                {
                    Id = Guid.NewGuid(),
                    Title = "Travel",
                    UrlSlug = "travel"
                },
                new Category
                {
                    Id = Guid.NewGuid(),
                    Title = "Food",
                    UrlSlug = "food"
                },
                new Category
                {
                    Id = Guid.NewGuid(),
                    Title = "Fashion",
                    UrlSlug = "fashion"
                },
                new Category
                {
                    Id = Guid.NewGuid(),
                    Title = "Sports",
                    UrlSlug = "sports"
                }
            };

            List<Tag> tags = new List<Tag>
            {
                new Tag
                {
                    Id = Guid.NewGuid(),
                    Title = "Technology"
                },
                new Tag
                {
                    Id = Guid.NewGuid(),
                    Title = "Travel"
                },
                new Tag
                {
                    Id = Guid.NewGuid(),
                    Title = "Food"
                },
                new Tag
                {
                    Id = Guid.NewGuid(),
                    Title = "Fashion"
                },
                new Tag
                {
                    Id = Guid.NewGuid(),
                    Title = "Sports"
                },
                new Tag
                {
                    Id = Guid.NewGuid(),
                    Title = "Music"
                },
                new Tag
                {
                    Id = Guid.NewGuid(),
                    Title = "Books"
                },
                new Tag
                {
                    Id = Guid.NewGuid(),
                    Title = "Art"
                },
                new Tag
                {
                    Id = Guid.NewGuid(),
                    Title = "Fitness"
                },
                new Tag
                {
                    Id = Guid.NewGuid(),
                    Title = "Nature"
                }
            };

            modelBuilder.Entity<Tag>().HasData(tags);
            modelBuilder.Entity<Category>().HasData(categories);
        }
    }
}
