﻿using Blogger.DataAccess.Repositories.Base_Repository;
using Blogger.DataAccess.Repositories.Interfaces;
using Blogger.Models.Common;

namespace Blogger.DataAccess.Repositories.Implements
{
    public class TagRepository : BaseRepository<Tag>, ITagRepository
    {
        public TagRepository(BloggerContext bloggerContext) : base(bloggerContext)
        {
        }
    }
}
