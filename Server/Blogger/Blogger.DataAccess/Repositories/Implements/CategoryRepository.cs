﻿using Blogger.DataAccess.Repositories.Base_Repository;
using Blogger.DataAccess.Repositories.Interfaces;
using Blogger.Models.Common;

namespace Blogger.DataAccess.Repositories.Implements
{
    public class CategoryRepository : BaseRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(BloggerContext bloggerContext) : base(bloggerContext)
        {
        }
    }
}
