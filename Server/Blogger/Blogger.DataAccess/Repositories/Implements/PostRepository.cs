﻿using Blogger.DataAccess.Repositories.Base_Repository;
using Blogger.DataAccess.Repositories.Interfaces;
using Blogger.Models.Common;

namespace Blogger.DataAccess.Repositories.Implements
{
    public class PostRepository : BaseRepository<Post>, IPostRepository
    {
        public PostRepository(BloggerContext bloggerContext) : base(bloggerContext)
        {
        }
    }
}
