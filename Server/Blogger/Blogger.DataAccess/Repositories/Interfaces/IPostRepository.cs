﻿using Blogger.DataAccess.Repositories.Base_Repository;
using Blogger.Models.Common;

namespace Blogger.DataAccess.Repositories.Interfaces
{
    public interface IPostRepository : IBaseRepository<Post>
    {
    }
}
