﻿using Blogger.Models.Common;
using Microsoft.EntityFrameworkCore;

namespace Blogger.DataAccess.Repositories.Base_Repository
{
    public class BaseRepository<T> : IBaseRepository<T> where T : BaseModel
    {
        private readonly BloggerContext bloggerContext;
        protected DbSet<T> entities;

        public BaseRepository(BloggerContext bloggerContext) {
            this.bloggerContext = bloggerContext;
            entities = this.bloggerContext.Set<T>();
        }
        public void Add(T entity)
        {
            entities.Add(entity);
        }

        public void Delete(T entity)
        {
            entities.Remove(entity);
        }

        public void GetAll()
        {
            entities.ToList();
        }

        public void Update(T entity)
        {
            entities.Update(entity);
        }
    }
}
