﻿using Blogger.Models.Common;

namespace Blogger.DataAccess.Repositories.Base_Repository
{
    public interface IBaseRepository<T>
    {
        void GetAll();
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}
