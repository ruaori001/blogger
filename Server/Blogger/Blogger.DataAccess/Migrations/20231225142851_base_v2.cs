﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Blogger.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class base_v2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "AmountOfVisit", "CreatedAt", "ModifiedAt", "Title", "UrlSlug" },
                values: new object[,]
                {
                    { new Guid("26887672-549d-4d1d-afac-009c4c8820e5"), 0, new DateTime(2023, 12, 25, 21, 28, 51, 351, DateTimeKind.Local).AddTicks(1285), new DateTime(2023, 12, 25, 21, 28, 51, 351, DateTimeKind.Local).AddTicks(1286), "Sports", "sports" },
                    { new Guid("35beb361-e6a6-41d7-8a36-e58a1534e6fb"), 0, new DateTime(2023, 12, 25, 21, 28, 51, 351, DateTimeKind.Local).AddTicks(1281), new DateTime(2023, 12, 25, 21, 28, 51, 351, DateTimeKind.Local).AddTicks(1282), "Food", "food" },
                    { new Guid("72371669-b02b-454d-8f95-8d1b72273d62"), 0, new DateTime(2023, 12, 25, 21, 28, 51, 351, DateTimeKind.Local).AddTicks(1238), new DateTime(2023, 12, 25, 21, 28, 51, 351, DateTimeKind.Local).AddTicks(1251), "Technology", "technology" },
                    { new Guid("c5246878-4dfd-4563-b14d-9fd61bda2040"), 0, new DateTime(2023, 12, 25, 21, 28, 51, 351, DateTimeKind.Local).AddTicks(1279), new DateTime(2023, 12, 25, 21, 28, 51, 351, DateTimeKind.Local).AddTicks(1279), "Travel", "travel" },
                    { new Guid("d091a358-25d0-41c1-915c-b8999a540f20"), 0, new DateTime(2023, 12, 25, 21, 28, 51, 351, DateTimeKind.Local).AddTicks(1283), new DateTime(2023, 12, 25, 21, 28, 51, 351, DateTimeKind.Local).AddTicks(1284), "Fashion", "fashion" }
                });

            migrationBuilder.InsertData(
                table: "Tags",
                columns: new[] { "Id", "AmountOfVisit", "CreatedAt", "ModifiedAt", "Title" },
                values: new object[,]
                {
                    { new Guid("2bf0e595-f9fb-4efd-9abb-1f9e714a1a8a"), 0, new DateTime(2023, 12, 25, 21, 28, 51, 351, DateTimeKind.Local).AddTicks(1301), new DateTime(2023, 12, 25, 21, 28, 51, 351, DateTimeKind.Local).AddTicks(1302), "Food" },
                    { new Guid("30befb7e-11bd-4f01-85cb-d48f9ea3609e"), 0, new DateTime(2023, 12, 25, 21, 28, 51, 351, DateTimeKind.Local).AddTicks(1303), new DateTime(2023, 12, 25, 21, 28, 51, 351, DateTimeKind.Local).AddTicks(1303), "Fashion" },
                    { new Guid("4f87ca79-7803-4067-82a3-d7e366997c72"), 0, new DateTime(2023, 12, 25, 21, 28, 51, 351, DateTimeKind.Local).AddTicks(1309), new DateTime(2023, 12, 25, 21, 28, 51, 351, DateTimeKind.Local).AddTicks(1309), "Books" },
                    { new Guid("50cc212a-ed36-4eac-9c83-6e9c05662f3d"), 0, new DateTime(2023, 12, 25, 21, 28, 51, 351, DateTimeKind.Local).AddTicks(1310), new DateTime(2023, 12, 25, 21, 28, 51, 351, DateTimeKind.Local).AddTicks(1311), "Art" },
                    { new Guid("554feee5-0433-49b6-92b8-b6e2573895f1"), 0, new DateTime(2023, 12, 25, 21, 28, 51, 351, DateTimeKind.Local).AddTicks(1305), new DateTime(2023, 12, 25, 21, 28, 51, 351, DateTimeKind.Local).AddTicks(1305), "Sports" },
                    { new Guid("7340853d-1bd1-401a-a5b3-52e62bc8db19"), 0, new DateTime(2023, 12, 25, 21, 28, 51, 351, DateTimeKind.Local).AddTicks(1300), new DateTime(2023, 12, 25, 21, 28, 51, 351, DateTimeKind.Local).AddTicks(1300), "Travel" },
                    { new Guid("7ad5c24a-605e-494c-922e-b5a1cdd161f6"), 0, new DateTime(2023, 12, 25, 21, 28, 51, 351, DateTimeKind.Local).AddTicks(1291), new DateTime(2023, 12, 25, 21, 28, 51, 351, DateTimeKind.Local).AddTicks(1291), "Technology" },
                    { new Guid("a2ff69f8-8816-4395-8698-78851d92ed39"), 0, new DateTime(2023, 12, 25, 21, 28, 51, 351, DateTimeKind.Local).AddTicks(1307), new DateTime(2023, 12, 25, 21, 28, 51, 351, DateTimeKind.Local).AddTicks(1307), "Music" },
                    { new Guid("cada55ba-4f6e-40c1-a8de-d3d055686022"), 0, new DateTime(2023, 12, 25, 21, 28, 51, 351, DateTimeKind.Local).AddTicks(1316), new DateTime(2023, 12, 25, 21, 28, 51, 351, DateTimeKind.Local).AddTicks(1317), "Nature" },
                    { new Guid("ec095813-9ff9-4899-bae9-acfb667c5277"), 0, new DateTime(2023, 12, 25, 21, 28, 51, 351, DateTimeKind.Local).AddTicks(1312), new DateTime(2023, 12, 25, 21, 28, 51, 351, DateTimeKind.Local).AddTicks(1312), "Fitness" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("26887672-549d-4d1d-afac-009c4c8820e5"));

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("35beb361-e6a6-41d7-8a36-e58a1534e6fb"));

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("72371669-b02b-454d-8f95-8d1b72273d62"));

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("c5246878-4dfd-4563-b14d-9fd61bda2040"));

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("d091a358-25d0-41c1-915c-b8999a540f20"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("2bf0e595-f9fb-4efd-9abb-1f9e714a1a8a"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("30befb7e-11bd-4f01-85cb-d48f9ea3609e"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("4f87ca79-7803-4067-82a3-d7e366997c72"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("50cc212a-ed36-4eac-9c83-6e9c05662f3d"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("554feee5-0433-49b6-92b8-b6e2573895f1"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("7340853d-1bd1-401a-a5b3-52e62bc8db19"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("7ad5c24a-605e-494c-922e-b5a1cdd161f6"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("a2ff69f8-8816-4395-8698-78851d92ed39"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("cada55ba-4f6e-40c1-a8de-d3d055686022"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("ec095813-9ff9-4899-bae9-acfb667c5277"));
        }
    }
}
